import React, { Component } from "react";
import MagicNumber from "./MagicNumber";

class SimpleForm extends Component {
  state = {
    number: 0
  };

  increase = () => {
    this.setState({ number: this.state.number + 1 });
  };

  decrease = () => {
    this.setState(prevState => ({
      number: prevState.number - 1
    }));
  };

  render() {
    return (
      <div style={{ textAlign: "center" }}>
        <h1>React is Fine!</h1>
        <MagicNumber number={this.state.number} />
        <button onClick={() => this.increase()}>Increase</button>
        <button onClick={this.decrease}>Decrease</button>
      </div>
    );
  }
}

export default SimpleForm;
