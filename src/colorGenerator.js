const FFF = 16777215;
const HEX = 16;

export const generate = () =>
  "#" + Math.floor(Math.random() * FFF).toString(HEX);
