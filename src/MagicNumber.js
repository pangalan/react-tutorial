import React, { Component } from "react";
import * as PropTypes from "prop-types";
import { generate } from "./colorGenerator";

const DELAY = 5000;
const DEFAULT_COLOR = "black";

class MagicNumber extends Component {
  state = {
    color: DEFAULT_COLOR
  };

  componentDidMount() {
    this.intervalId = setInterval(this.setRandomColor, DELAY);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.number !== this.props.number) {
      this.setState({ color: DEFAULT_COLOR });
    }
  }

  setRandomColor = () => {
    this.setState({
      color: generate()
    });
  };

  render() {
    return <h4 style={{ color: this.state.color }}>{this.props.number}</h4>;
  }
}

MagicNumber.propTypes = {
  number: PropTypes.number
};

export default MagicNumber;
